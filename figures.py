#! /usr/bin/env python3
'''Some classes describing plane figures, written with the Billiard in mind.

Cicles and Rectangles are useful as building blocks of more interesting
Figures.
'''

from billiard import PRECISION, STEP, distance
from math import sqrt, acos, atan
from numpy import sin, cos, pi, array, arange, abs, \
    linspace, ones, concatenate


class Figure(object):
    '''An abstract Figure.
    '''
    def __init__(self, *args):
        pass

    def is_outside(self, point):
        '''Note that outside includes the boundary points.
        '''
        raise NotImplementedError

    def is_inside(self, point):
        '''Note that inside is an open set (excludes the boundary).
        '''
        return not self.is_outside(point)

    def reflection_angle(self, point, incidence):
        '''Returns the angle at which a free particle is reflected when
        it hits the point at angle incidence. Point should lie on the
        figure's boundary, up to PRECISION.
        '''
        raise NotImplementedError

    _border = None

    @property
    def border(self):
        raise NotImplementedError

    _bounds = None

    @property
    def bounds(self):

        '''Returns an array of two diagonally opposite vertices of the
        figures minimal bounding rectangle:

            array([[left, bottom], [right, top]])

        I don't instantiate a Rectangle here for sake of economy.
        '''

        if self._bounds is None:

            border = ([self.border] if not isinstance(self.border, list)
                      else self.border)
            bottom = min(points[:, 1].min() for points in border)
            top = max(points[:, 1].max() for points in border)
            left = min(points[:, 0].min() for points in border)
            right = max(points[:, 0].max() for points in border)
            self._bounds = array([[left, bottom], [right, top]])
        return self._bounds


class Circle(Figure):
    def __init__(self, radius=1., center=(0., 0.)):
        self.radius = radius
        assert len(center) == 2
        self.center = array(center)

    def is_outside(self, point):
        return sum((point - self.center)**2) >= self.radius**2

    def reflection_angle(self, point, incidence):
        x = (point[0] - self.center[0]) / self.radius
        x = min(x, 1) if x > 0 else max(x, -1)
        polar = acos(x)  # point assumed on the boundary up to some precision
        if point[1] - self.center[1] < 0:
            polar = -polar
        return pi + 2 * polar - incidence

    @property
    def border(self):
        Φ = linspace(0., 2 * pi, int(2 * pi * self.radius / STEP))
        return self.center + self.radius * array([cos(Φ), sin(Φ)]).T


class Rectangle(Figure):
    '''We describe a Rectangle by two diagonally opposite vertices.
    '''
    def __init__(self, vertex1=(0., 0.), vertex3=(1., .5)):
        if (isinstance(vertex1, (float, int))
                and isinstance(vertex3, (float, int))):
            xs, ys = [0., vertex1], [0., vertex3]
        else:
            xs, ys = [vertex1[0], vertex3[0]], [vertex1[1], vertex3[1]]
        self.top, self.bottom = max(ys), min(ys)
        self.left, self.right = min(xs), max(xs)
        self.vertices = array([[self.left, self.bottom],
                               [self.right, self.bottom],
                               [self.right, self.top],
                               [self.left, self.top]])

    def is_outside(self, point):
        return (point[1] <= self.bottom
                or point[1] >= self.top
                or point[0] >= self.right
                or point[0] <= self.left
                )

    def reflection_angle(self, point, incidence):
        if any(distance(point, v) < PRECISION for v in self.vertices):
            return pi + incidence  # avoid getting trapped in corner
        if (abs(point[0] - self.left) < PRECISION
                or abs(point[0] - self.right) < PRECISION):
            return pi - incidence
        if (abs(point[1] - self.bottom) < PRECISION
                or abs(point[1] - self.top) < PRECISION):
            return -incidence
        return incidence

    @property
    def border(self):
        xs = arange(self.left, self.right, STEP)
        ys = arange(self.bottom, self.top, STEP)
        return concatenate((
            array([xs, self.bottom * ones(xs.shape)]).T,
            array([self.right * ones(ys.shape), ys]).T,
            array([xs[::-1], self.top * ones(xs.shape)]).T,
            array([self.left * ones(ys.shape), ys[::-1]]).T
            ))


class Sinai(Figure):
    '''The subset of the plane between a Circle (outer) and a square
    (special case of Rectangle). A notorious chaotic Billiard.
    '''
    def __init__(self, edge=1.5, radius=.25):
        if radius > edge / 2:
            raise ValueError("circle doesn't fit inside.")
        self.inner = Circle(radius)
        self.outer = Rectangle([-edge/2, -edge/2], [edge/2, edge/2])

    def is_outside(self, point):
        return self.outer.is_outside(point) or self.inner.is_inside(point)

    def reflection_angle(self, point, incidence):
        nextp = point + 2 * STEP * array([cos(incidence), sin(incidence)])
        if self.outer.is_outside(nextp):
            return self.outer.reflection_angle(point, incidence)
        if self.inner.is_inside(nextp):
            return pi + self.inner.reflection_angle(point, pi + incidence)
        return incidence

    @property
    def border(self):
        return [self.inner.border, self.outer.border]


class Stadium(Figure):
    '''The Bunimovich stadium, built with a Rectangle and two halves of
    Circles. A notorious chaotic Billiard.
    '''
    def __init__(self, width=2., height=1.5):
        '''*width* is the width of the straight horizontal edges, excluding
        the caps.
        '''
        self.width = width
        self.height = height
        self.rectangle = Rectangle([-width / 2, 0], [width / 2, height])
        self.caps = (Circle(height / 2, [-width / 2, height / 2]),
                     Circle(height / 2, [width / 2, height / 2])
                     )

    def is_outside(self, point):
        return (self.rectangle.is_outside(point)
                and all(x.is_outside(point) for x in self.caps)
                )

    def reflection_angle(self, point, incidence):
        if (abs(point[1]) < PRECISION or
                abs(point[1] - self.height) < PRECISION):
            return self.rectangle.reflection_angle(point, incidence)
        if point[0] < -self.width / 2:
            return self.caps[0].reflection_angle(point, incidence)
        if point[0] > self.width / 2:
            return self.caps[1].reflection_angle(point, incidence)

    @property
    def border(self):
        width, height = self.width, self.height
        br = self.rectangle.border
        bcl = self.caps[0].border
        bcr = self.caps[1].border
        b1 = br[br[:, 1] == 0][:-1]
        b2 = bcr[bcr[:, 0] > width / 2]
        b2 = array(sorted(b2, key=lambda p: p[1]))
        b3 = br[br[:, 1] == height]
        b4 = bcl[bcl[:, 0] < -width / 2]
        b4 = array(sorted(b4, key=lambda p: -p[1]))
        return concatenate((b1, b2, b3, b4))


class Ring(Figure):
    '''The subset of the plane between two (in general non-concentric)
    circles.
    '''
    def __init__(self, r_outer=1., r_inner=.4, displacement=(-.31, 0.)):
        if isinstance(displacement, (float, int)):
            displacement = array((float(displacement), 0.))
        elif len(displacement) == 2:
            displacement = array(displacement)
        else:
            raise ValueError(
                'figures.Ring: displacement should be a point or a number')
        if r_outer < r_inner + distance(displacement, [0, 0]):
            raise ValueError("inner circle won't fit inside outer")
        self.outer = Circle(r_outer)
        self.inner = Circle(r_inner, displacement)

    def is_outside(self, point):
        return self.outer.is_outside(point) or self.inner.is_inside(point)

    def reflection_angle(self, point, incidence):
        nextp = point + 2 * STEP * array([cos(incidence), sin(incidence)])
        if self.outer.is_outside(nextp):
            return self.outer.reflection_angle(point, incidence)
        if self.inner.is_inside(nextp):
            return pi + self.inner.reflection_angle(point, pi + incidence)
        return incidence

    @property
    def border(self):
        return [self.outer.border, self.inner.border[::-1]]


class Eight(Figure):

    def __init__(self, radius=.51, spread=1.):
        assert spread < 2 * radius
        self.radius, self.spread = radius, spread
        self.circleL = Circle(radius=radius, center=(-spread / 2, 0))
        self.circleR = Circle(radius=radius, center=(spread / 2, 0))

    def is_outside(self, point):
        return (self.circleL.is_outside(point)
                and self.circleR.is_outside(point))

    def reflection_angle(self, point, incidence):
        if distance(point, self.circleL.center) - self.radius < PRECISION:
            return self.circleL.reflection_angle(point, incidence)
        if distance(point, self.circleR.center) - self.radius < PRECISION:
            return self.circleR.reflection_angle(point, incidence)
        raise ValueError(
            f'figures.Eight: point {point} not at figure boundary')

    _border = None

    @property
    def border(self):
        if self._border is None:
            b1, b2 = self.circleL.border, self.circleR.border
            b = concatenate((b2, b1))
            filterfn = (lambda p: self.circleL.is_outside(p)
                        and self.circleR.is_outside(p))
            d = self.spread + 2 * self.radius
            keyfn = (lambda p: -p[0] if p[1] > 0 else p[0] + d)
            self._border = array(sorted(list(filter(filterfn, b)), key=keyfn))
        return self._border


class Ellipse(Figure):

    def __init__(self, semiaxes=(1., .9)):

        assert len(semiaxes) == 2 and all(x > 0 for x in semiaxes)
        b, a = sorted(semiaxes)
        c = sqrt(a ** 2 - b ** 2)
        self.foci = array([[-c, 0], [c, 0]])
        self.a, self.b, self.c = a, b, c
        # print(a, b, c)

    def is_outside(self, point):
        return (distance(point, self.foci[0]) + distance(point, self.foci[1])
                >= 2 * self.a)

    @property
    def border(self):
        if self._border is None:
            t = arange(0, 2 * pi, STEP / self.c)
            self._border = array([self.a * cos(t), self.b * sin(t)]).T
        return self._border

    def reflection_angle(self, point, incidence):
        x, y = point
        if y == 0:
            theta = pi / 2 if x < 0 else -pi / 2
        else:
            m = -x / y * (self.b / self.a) ** 2
            theta = atan(m)
        return (2 * theta - incidence) % (2 * pi)


__all__ = ['Figure', 'Circle', 'Rectangle', 'Sinai', 'Stadium',
           'Ring', 'Eight', 'Ellipse', 'figures']
figures = [Circle, Rectangle, Sinai, Stadium, Ring, Eight, Ellipse]
