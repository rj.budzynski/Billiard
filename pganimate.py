#! /usr/bin/python3

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow, QApplication
# from PyQt5.QtGui import QPainter
# from PyQt5.QtGui import QGraphicsPathItem, QPolygonF, QPainterPath
import pyqtgraph as pg
from numpy import array, pi, concatenate
from numpy.random import rand

from figures import figures
from billiard import Billiard

pg.setConfigOption('background', '#2a2b35')
pg.setConfigOption('foreground', 0.98)


class GuiBilliard(Billiard, pg.PlotWidget):

    bounces = 0
    oldLineParams = {
        'pen': pg.mkPen(width=3, color=.3, style=3),
        'autoDownsample': True,
        # 'antialias': True
    }
    newLineParams = {
        'pen': pg.mkPen(width=3, color=.5, style=2),
        'autoDownsample': True,
        # 'antialias': True
    }
    ballParams = {
        'pen': None,
        'symbol': 'o',
        'symbolBrush': pg.mkBrush(color='r')
    }
    startBallParams = {
        'pen': None,
        'symbol': 'o',
        'symbolBrush': pg.mkBrush(color='m')
    }
    borderLineParams = {
        'pen': pg.mkPen(width=4, color='y'),
        'antialias': True
    }
    figureFillParams = {
        'brush': pg.mkBrush(color=(50, 52, 64))
    }  # not used so far, TODO: figure out how to fill figure interior

    @property
    def running(self):
        return self.timer.isActive()

    def __init__(self, figure):
        Billiard.__init__(self, figure)
        pg.PlotWidget.__init__(self)
        self.timer = QTimer()

    def setup(self, pointStart=None, angleStart=None, speed=.5, interval=40):
        self.setAspectLocked(True)
        self.setTitle(
            f"{self.figure.__class__.__name__} Billiard : "
            "<b>stopped</b> : "
            "hit Space to start"
        )
        for ax in 'left', 'bottom':
            self.hideAxis(ax)
        # borderPen = pg.mkPen(width=4, color='y')  # , antialias=True)
        if isinstance(self.figure.border, list):
            for b in self.figure.border:
                it = self.plot(b, **self.borderLineParams)
                it.setZValue(1777777)
            # KLUDGE ALERT!!!
            # print(self.listDataItems()[-2:])
            fill = FigureBackgroundItem(self, *self.listDataItems()[-2:])
        else:
            it = self.plot(self.figure.border, **self.borderLineParams)
            it.setZValue(1777777)
            it0 = self.plot(self.figure.border[0:2])
            fill = FigureBackgroundItem(self, it, it0)
        fill.setBrush(self.figureFillParams['brush'])
        self.addItem(fill)
        fill.setZValue(0)
        [[xmin, ymin], [xmax, ymax]] = self.figure.bounds
        if pointStart is None:
            width, height = xmax - xmin, ymax - ymin
            while True:
                pointStart = (
                    array([xmin, ymin]) + array([width, height]) * rand(2))
                if self.figure.is_inside(pointStart):
                    break
        self.old_points, angle = self.run(pointStart, angleStart, 1)
        self.points, self.angle = self.run(self.old_points[-1], angle, 1)
        self.plot(array([pointStart]), **self.startBallParams)
        self.plot(self.old_points, **self.oldLineParams)
        self.plot(self.points, **self.newLineParams)
        self.plot(self.points[-1:], **self.ballParams)
        self.autoRange()
        text = pg.TextItem('© 2018 Robert J Budzyński', anchor=(0., -.2))
        text2 = pg.TextItem(f'{self.bounces} bounces', anchor=(1., -.2))
        self.counterLabel = text2
        self.addItem(text)
        self.addItem(text2)
        text.setPos(xmin, ymin)
        text2.setPos(xmax, ymin)
        self.height = ymax - ymin
        self.timer.timeout.connect(self.update)
        self.interval, self.speed = interval, speed

    def update(self):

        step = self.speed * self.interval / 1000 * self.height
        more_points, new_angle = self.run(self.points[-1], self.angle,
                                          step=step)
        if new_angle != self.angle:
            self.bounces += 1
            # print(f'bounced {self.bounces} times', end='\r', flush=True)
            self.angle = new_angle
            self.old_points = concatenate(
                                (self.old_points,
                                 self.points[:-1],
                                 more_points[:-1]))
            self.points = more_points[-1:]
            self.listDataItems()[-3].setData(self.old_points)
            self.counterLabel.setText(
                f'{self.bounces} bounces; '
                f'{len(self.old_points)} track segments')
        else:
            self.points = concatenate((self.points, more_points))
        line, ball = self.listDataItems()[-2:]
        line.setData(self.points)
        ball.setData(self.points[-1:])

    def startTimer(self):
        self.timer.start(self.interval)
        self.disableAutoRange()  # gets rid of some flicker

    def stopTimer(self):
        self.timer.stop()

    def keyPressEvent(self, event):
        text = event.text()
        if text == 'f':
            if not self.running:
                self.update()
        elif text == ' ':
            if self.running:
                self.timer.stop()
                self.setTitle(
                    f"{self.figure.__class__.__name__} Billiard : "
                    "<b>stopped</b> : "
                    "hit Space to restart"
                )
            else:
                self.timer.start(self.interval)
                self.setTitle(
                    f"{self.figure.__class__.__name__} Billiard : "
                    "<b>running</b> : "
                    "hit Space to pause"
                )
        elif text == '+':
            self.speed += .05
        elif text == '-':
            self.speed -= .05
            if self.speed < .05:
                self.speed = .05
        elif text == 'c':
            was_running = self.running
            if was_running:
                self.stopTimer()
            self.old_points, self.points = self.points[-1:], self.points[-1:]
            self.listDataItems()[-3].setData(self.old_points)
            self.listDataItems()[-4].setData(self.points[-1:])
            self.bounces = 0
            if was_running:
                self.startTimer()
        else:
            event.ignore()


class FigureBackgroundItem(pg.FillBetweenItem):

    def __init__(self, widget, *args):
        pg.FillBetweenItem.__init__(self, *args)
        self.widget = widget

    def mouseClickEvent(self, event):
        print(f'mouse click at {event.pos()}')
        # print(f'parent: {self.widget}')
        self.widget.pointStart = event.pos()
        event.accept()


if __name__ == '__main__':
    from sys import argv, exit

    try:
        for fig_class in figures:
            if fig_class.__name__ == argv[1]:
                break
        else:
            exit(f'{argv[0]}: {argv[1]}: unknown Figure.\n'
                 f'Please specify one of:\n'
                 f'\t{" ".join(x.__name__ for x in figures)}')
    except IndexError:
        fig_class = figures[-1]  # better a default than a required arg

    if argv[2:]:
        fig = fig_class(*[float(x) for x in argv[2:]])
    else:
        fig = fig_class()  # figure classes should have default geometries

    app = QApplication(argv)

    angle = angleStart = rand() * 2 * pi
    billiard = GuiBilliard(fig)

    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 600)
    mw.setWindowTitle('Billiard via PyQtGraph')
    mw.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore()
    )
    mw.setCentralWidget(billiard)
    mw.setContentsMargins(5, 5, 5, 5)
    billiard.setup(angleStart=angleStart)
    mw.statusBar().showMessage(
        "Press 'q' to quit; Space to run/pause; "
        "'f' to single-step (when paused); "
        "'+ / -' to speed up / slow down; "
        "'c' to clear track")
    mw.show()
    # billiard.startTimer()

    exit(app.exec())
