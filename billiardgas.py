#! /usr/bin/python3

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QMainWindow
from numpy import array, pi
from numpy.random import random
import pyqtgraph as pg

from operator import itemgetter
from functools import partial


from billiard import Billiard
from figures import figures

pg.setConfigOption('background', '#2a2b35')
pg.setConfigOption('foreground', 0.98)


class BilliardGas(Billiard):

    pass


class GuiBilliardGas(pg.PlotWidget):

    interval = 40  # giving 25 fps
    rate = 7  # speed is STEP * rate / .04 = .45 units / s
    speed = .1  # .1 figure height / s
    nParticles = 13

    @property
    def step(self):
        return self.speed * self.height * self.interval / 1000

    ballParams = {
        'pen': None,
        'symbol': 'o',
        'symbolSize': 5,
        'pxMode': True,
        'symbolBrush': pg.mkBrush(color='0ff'),
        'symbolPen': pg.mkPen(None)
    }

    borderLineParams = {'pen': pg.mkPen(width=4, color='y'), 'antialias': True}

    figureFillParams = {'brush': pg.mkBrush(color=(60, 64, 75))}

    @property
    def running(self):
        return self.timer.isActive()

    def __init__(self, figure):

        pg.PlotWidget.__init__(self)
        self.figure = figure
        self.timer = QTimer()
        self.setAspectLocked(True)
        self.setTitle("Gas of noninteracting particles inside a "
                      f"{self.figure.__class__.__name__} : "
                      "<b>stopped</b> : "
                      "hit Space to start")
        for ax in 'left', 'bottom':
            self.hideAxis(ax)
        # borderPen = pg.mkPen(width=4, color='y')  # , antialias=True)
        if isinstance(self.figure.border, list):
            if len(self.figure.border) == 2:
                for b in self.figure.border:
                    it = self.plot(b, **self.borderLineParams)
                    it.setZValue(17)
                # KLUDGE ALERT!!!
                # print(self.listDataItems()[-2:])
                fill = FigureBackgroundItem(self, *self.listDataItems()[-2:])
            else:  # TODO figure it out someday
                pass
        else:
            it = self.plot(self.figure.border, **self.borderLineParams)
            it.setZValue(17)
            it0 = self.plot(self.figure.border[0:2])
            fill = FigureBackgroundItem(self, it, it0)
        fill.setBrush(self.figureFillParams['brush'])
        self.addItem(fill)
        fill.setZValue(0)
        self.autoRange()
        text = pg.TextItem('© 2018 Robert J Budzyński', anchor=(0., -.2))
        self.addItem(text)
        bounds = self.figure.bounds
        xmin, ymin = bounds[0, 0], bounds[0, 1]
        text.setPos(xmin, ymin)

    def setup(self, nParticles=None, speed=None, fps=None):

        # if rate is not None:
        #     self.rate = rate
        if fps is not None:
            self.interval = 1000 / fps
        if nParticles is not None:
            self.nParticles = nParticles
        nParticles = self.nParticles
        ((xmin, ymin), (xmax, ymax)) = self.figure.bounds
        self.height = ymax - ymin
        if speed is not None:
            self.speed = speed
        # self.step = self.speed * height * self.interval / 1000
        points, p0, S = [], array((xmin, ymin)), array(
            ((xmax - xmin) / 2, ymax - ymin))
        while len(points) < nParticles:
            p = p0 + random(2) * S
            if self.figure.is_inside(p):
                points.append(p)
        self.points = array(points)
        self.angles = pi * (.4 + .4 * random(nParticles))
        self.billiard = Billiard(self.figure)
        dots = self.plot(self.points, **self.ballParams)
        dots.setZValue(7)
        text2 = pg.TextItem(
            f'{nParticles} particles; speed: {self.speed:.2f} '
            'figure heights ps; '
            f'fps: {1000/self.interval:.2f}',
            anchor=(1., -.2))
        self.addItem(text2)
        text2.setPos(xmax, ymin)
        self.infoLabel = text2
        self._run = partial(self.billiard.run, step=self.step)
        self.dots = dots
        self.timer.timeout.connect(self.update)

    _getlast = itemgetter(-1)

    def update(self):
        tracks, self.angles[:] = zip(*map(
             self._run, self.points, self.angles))
        self.points[:] = tuple(map(self._getlast, tracks))
        self.dots.setData(self.points)

    def startTimer(self):
        self.timer.start(self.interval)
        self.disableAutoRange()  # gets rid of some flicker

    def stopTimer(self):
        self.timer.stop()

    def keyPressEvent(self, event):
        text = event.text()
        if text == 'f':
            if not self.running:
                self.update()
        elif text == ' ':
            if self.running:
                self.timer.stop()
                self.setTitle("Gas of noninteracting particles inside a "
                              f"{self.figure.__class__.__name__} : "
                              "<b>stopped</b> : "
                              "hit Space to start")
            else:
                self.timer.start(self.interval)
                self.setTitle("Gas of noninteracting particles inside a "
                              f"{self.figure.__class__.__name__} : "
                              "<b>running</b> : "
                              "hit Space to pause")
        elif text == '+':
            was = self.running
            self.stopTimer()
            self.speed += .01
            self._run = partial(self.billiard.run, step=self.step)
            self.infoLabel.setText(
                f'{len(self.points)} particles; speed: {self.speed:.2f} '
                'figure heights ps; '
                f'fps: {1000/self.interval:.2f}')
            if was:
                self.startTimer()
        elif text == '-':
            was = self.running
            self.stopTimer()
            self.speed = max(self.speed - .01, .01)
            self._run = partial(self.billiard.run, step=self.step)
            self.infoLabel.setText(
                f'{len(self.points)} particles; speed: {self.speed:.2f} '
                'figure heights ps; '
                f'fps: {1000/self.interval:.2f}')
            if was:
                self.startTimer()
        elif text == 'c':
            self.stopTimer()
            for it in self.dots, self.infoLabel:
                self.getPlotItem().removeItem(it)
            self.setup()
        else:
            event.ignore()


class FigureBackgroundItem(pg.FillBetweenItem):
    def __init__(self, widget, *args):
        pg.FillBetweenItem.__init__(self, *args)
        self.widget = widget

    def mouseClickEvent(self, event):
        print(f'mouse click at {event.pos()}')
        # print(f'parent: {self.widget}')
        # self.widget.pointStart = event.pos()
        event.accept()


if __name__ == '__main__':
    from sys import argv, exit

    app = QApplication(argv)

    fig_class = figures[-1]  # better a default than a required arg
    nParticles, fps, speed = None, None, None

    figure_names = [f.__name__ for f in figures]
    args = []
    for a in argv[1:]:
        if a in figure_names:
            fig_class = figures[figure_names.index(a)]
        elif a.startswith('n='):
            nParticles = int(a.split('=')[1])
        elif a.startswith('fps='):
            fps = float(a.split('=')[1])
        elif a.startswith('speed='):
            speed = int(a.split('=')[1])
        else:
            args.append(a)

    if args:
        fig = fig_class(*[float(x) for x in args])
    else:
        fig = fig_class()
    widget = GuiBilliardGas(fig)
    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 600)
    mw.setWindowTitle('Billiard Gas via PyQtGraph')
    mw.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    mw.setCentralWidget(widget)
    mw.setContentsMargins(5, 5, 5, 5)
    widget.setup(fps=fps, speed=speed, nParticles=nParticles)
    mw.statusBar().showMessage("Press 'q' to quit; Space to run/pause; "
                               "'f' to single-step (when paused); "
                               "'+ / -' to speed up / slow down; "
                               "'c' to reset")

    mw.show()

    exit(app.exec())
