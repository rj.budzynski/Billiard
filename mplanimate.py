#! /usr/bin/python3

import matplotlib
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from matplotlib import interactive
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure as MplFigure
from matplotlib.animation import FuncAnimation
from matplotlib.text import Text
from PyQt5.QtWidgets import QMainWindow, QApplication, QSizePolicy
from numpy import array, pi, concatenate
from numpy.random import rand
from sys import argv, exit

from figures import figures
from billiard import Billiard


class MplBilliard(FigureCanvas):

    _running = False

    def __init__(self, figure, parent=None):

        mfig = MplFigure(figsize=(8, 6), dpi=100, facecolor='#2a2b35')
        self.axes = mfig.add_subplot(111)
        # print(self.axes)
        self.axes.set_position((.01, .0, .98, .95))
        self.billiard = Billiard(figure)
        FigureCanvas.__init__(self, mfig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def setup(self, pointStart, angleStart, rate=15, interval=40):

        interactive(True)
        axs = self.axes
        self._title = axs.set_title(
            f"{self.billiard.figure.__class__.__name__} Billiard : "
            # "stopped : "
            "hit Space to run / pause",
            color='w')

        axs.set_aspect(1)
        axs.grid(False)
        axs.axis('off')

        lines = []
        if isinstance(self.billiard.figure.border, list):
            for b in self.billiard.figure.border:
                lines.extend(
                    axs.plot(*b.T, linewidth=3., color='yellow', zorder=9))
        else:
            lines.extend(
                axs.plot(
                    *self.billiard.figure.border.T,
                    linewidth=3.,
                    color='yellow',
                    zorder=9))
        points, self.angle = self.billiard.run(pointStart, angleStart, 5)
        self.old_points, self.points = points[:-1], points[-1:]
        lines.extend(axs.plot(
            *array([pointStart]).T, linestyle='None', marker='o',
            markerfacecolor='magenta', markersize=7.)
        )
        lines.extend(axs.plot(
            *self.old_points.T, linestyle=':', linewidth=2., color='0.3')
        )
        lines.extend(axs.plot(
            *self.points.T, linestyle=':', linewidth=2., color='0.5')
        )
        lines.extend(axs.plot(
            *self.points[-1:].T, linestyle='None', marker='o',
            markerfacecolor='red', markersize=7., zorder=99)
        )
        axs.relim()
        self.figure.text(
            .98, .02,
            '© 2018 Robert J Budzyński',
            color='.7',
            ha='right',
            figure=self.figure)
        self.lines = lines
        self.interval, self.rate = interval, rate
        # for l in lines[-5:]:
        #     l.set_animated(True)  # not sure what this actually does
        self._anim = FuncAnimation(
            self.figure,
            self.update_anim,
            init_func=self._init_anim,
            blit=True,
            interval=self.interval)

    def update_anim(self, k):
        # print(f'k = {k}')
        # print(f'points = {self.points}')
        lines = self.lines[-5:]
        if not k:
            self._anim.event_source.stop()
        if self._running:
            more_points, new_angle = self.billiard.run(self.points[-1],
                                                        self.angle, self.rate)
            if new_angle != self.angle:
                self.angle = new_angle
                self.old_points = concatenate((self.old_points, more_points[:-1]))
                self.points = more_points[-1:]
                self.lines[-3].set_data(self.old_points.T)
                lines = self.lines[:]
                # lines += self.lines[-3],
            else:
                self.points = concatenate((self.points, more_points))
            line, ball = self.lines[-2:]
            line.set_data(self.points.T)
            ball.set_data(self.points[-1:].T)
            # lines += line, ball
        return lines

    def _stop(self):
        if self._running:
            self._anim.event_source.stop()
            self._running = False

    def _start(self):
        if not self._running:
            self._anim.event_source.start()
            self._running = True

    def _init_anim(self):
        return self.lines[-4:]  # + [self._title]

    def handle_key(self, event):
        text = event.text()
        # print(f'key event: {text}')
        if text == ' ':
            if self._running:
                self._title.set_text(
                        f"{self.billiard.figure.__class__.__name__} Billiard : "
                        "stopped : "
                        "hit Space to resume"
                )
                self._stop()
            else:
                self._title.set_text(
                    f"{self.billiard.figure.__class__.__name__} Billiard : "
                    "running : "
                    "hit Space to pause"
                )
                self._start()
        elif text == '+':
            self._stop()
            self.rate += 3
            self._start()
        elif text == '-':
            self._stop()
            self.rate -= 3
            if self.rate <= 0:
                self.rate = 1
            self._start()
        elif text == 'c':
            self._stop()
            self.old_points = self.points[-5:-1]
            self.points = self.points[-1:]
            self.lines[-2].set_data(self.points.T)
            self.lines[-3].set_data(self.old_points.T)
            self.lines[-4].set_data(self.points[-1:].T)
            self._start()
        # elif text == 'f':
        #     if not self._running:
        #         self.update_anim(1)
        #         self.draw()
        elif text == 'q':
            app.quit()
        else:
            pass


if __name__ == '__main__':

    try:
        for fig_class in figures:
            if fig_class.__name__ == argv[1]:
                break
        else:
            exit(f'{argv[0]}: {argv[1]}: unknown Figure.\n'
                 f'Please specify one of:\n'
                 f'\t{" ".join(x.__name__ for x in figures)}')
    except IndexError:
        fig_class = figures[-1]  # better a default than a required arg

    if argv[2:]:
        fig = fig_class(*[float(x) for x in argv[2:]])
    else:
        fig = fig_class()  # figure classes should have default geometries

    app = QApplication(argv)

    pointStart = (.3, .3)
    # points = array([pointStart])
    angle = angleStart = rand() * 2 * pi

    billiard = MplBilliard(fig)

    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 600)
    mw.setWindowTitle('Billiard via Matplotlib')
    mw.keyPressEvent = billiard.handle_key
    mw.setCentralWidget(billiard)
    mw.setContentsMargins(5, 5, 5, 5)
    billiard.setup(pointStart, angleStart)
    mw.statusBar().showMessage(
        "Press 'q' to quit; Space to run/pause; "
        # "'f' to single-step (when paused); "
        "'+ / -' to speed up / slow down; "
        "'c' to clear track")
    mw.show()
    # billiard.startTimer()

    exit(app.exec())
