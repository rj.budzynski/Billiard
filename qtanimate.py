#! /usr/bin/python3

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.Qt import QPointF, QPainter, QColor
from PyQt5.QtChart import QChart, QChartView, QLineSeries, QScatterSeries

from numpy import array, pi, concatenate
from numpy.random import rand

from figures import figures
from billiard import Billiard


class GuiBilliard(Billiard, QChart):

    padding = .05

    def __init__(self, figure):
        Billiard.__init__(self, figure)
        QChart.__init__(self)
        self.timer = QTimer()

    def applyDefaultStyle(self):
        self.setTheme(QChart.ChartThemeDark)
        self.legend().hide()
        self.setDropShadowEnabled(True)

    def setXrange(self, xmin, xmax):
        self.axisX().setRange(xmin, xmax)

    def setYrange(self, ymin, ymax):
        self.axisY().setRange(ymin, ymax)

    def setPlotData(self, points, index=-1):
        series = self.series()[index]
        series.replace([QPointF(x, y) for x, y in points])

    def plot(self,
             points,
             lineWidth=None,
             lineColor=None,
             lineStyle=None,
             xGrid=None,
             yGrid=None):
        index = len(self.series())
        self.addSeries(QLineSeries())
        self.setPlotData(points, index)
        if lineWidth or lineStyle:  # yes, it has to be this cumbersome
            series = self.series()[index]
            pen = series.pen()
            if lineWidth:
                pen.setWidth(lineWidth)
            if lineStyle:
                pen.setStyle(lineStyle)
            series.setPen(pen)
        if lineColor:
            self.series()[index].setColor(QColor(lineColor))
        if xGrid is not None:
            self.axisX().setGridLineVisible(bool(xGrid))
        if yGrid is not None:
            self.axisY().setGridLineVisible(bool(yGrid))

    def setup(self, pointStart, angleStart, rate=15, interval=40):

        self.applyDefaultStyle()
        self.setTitle(f"{self.figure.__class__.__name__} Billiard : "
                      "<b>stopped</b> : "
                      "hit Space to run")
        if isinstance(self.figure.border, list):
            for b in self.figure.border:
                self.plot(b, lineWidth=4, lineColor='yellow')
                X, Y = concatenate(self.figure.border).T
        else:
            self.plot(self.figure.border, lineWidth=4, lineColor='yellow')
            X, Y = self.figure.border.T
        points, self.angle = self.run(pointStart, angleStart, 2)
        startSeries = QScatterSeries()
        startSeries.append(*pointStart)
        startSeries.setMarkerSize(12.)
        startSeries.setColor(QColor('magenta'))
        startSeries.setBorderColor(QColor('magenta'))
        self.addSeries(startSeries)
        self.old_points = points[:1]
        self.plot(
            self.old_points, lineWidth=3., lineColor='#404040', lineStyle=3)
        self.points = points[1:]
        self.plot(self.points, lineWidth=3., lineColor='#808080', lineStyle=3)
        ball = QScatterSeries()
        ball.append(*self.points[-1])
        ball.setMarkerSize(12.)
        ball.setColor(QColor('red'))
        ball.setBorderColor(QColor('magenta'))
        self.addSeries(ball)
        self.createDefaultAxes()
        minX, maxX, minY, maxY = min(X), max(X), min(Y), max(Y)
        dX, dY = (1 + self.padding) * (maxX - minX), (1 + self.padding) * (
            maxY - minY)
        X0, Y0 = (maxX + minX) / 2, (maxY + minY) / 2
        self.setXrange(X0 - dX / 2, X0 + dX / 2)
        self.setYrange(Y0 - dY / 2, Y0 + dY / 2)
        self.axisX().setGridLineVisible(False)
        self.axisY().setGridLineVisible(False)
        self.axisX().setVisible(False)
        self.axisY().setVisible(False)

        self.chartView = QChartView(self)
        self.chartView.setRenderHint(QPainter.Antialiasing)
        self.chartView.keyPressEvent = self.keyPressEvent
        self.timer.timeout.connect(self.update)
        self.rate, self.interval = rate, interval

        # self.resize(700, 700)

    def startTimer(self):
        self.timer.start(self.interval)

    def stopTimer(self):
        self.timer.stop()

    def update(self):
        more_points, new_angle = self.run(
                                    self.points[-1], self.angle,
                                    self.rate)
        if new_angle != self.angle:
            self.old_points = concatenate((self.old_points, self.points))
            self.setPlotData(self.old_points, -3)
            self.points = more_points
            self.angle = new_angle
        else:
            self.points = concatenate((self.points, more_points))
        self.setPlotData(self.points, -2)
        self.setPlotData(self.points[-1:], -1)

    def keyPressEvent(self, event):
        text = event.text()
        # print(f'key pressed: {text}')
        if text == 'f':
            if not self.timer.isActive():
                self.update()
        elif text == ' ':
            if self.timer.isActive():
                self.timer.stop()
                self.setTitle(f"{self.figure.__class__.__name__} Billiard : "
                              "<b>stopped</b> : "
                              "hit Space to restart")
            else:
                self.timer.start(self.interval)
                self.setTitle(f"{self.figure.__class__.__name__} Billiard : "
                              "<b>running</b> : "
                              "hit Space to pause")
        elif text == '+':
            self.stopTimer()
            self.rate += 2
            self.startTimer()
        elif text == '-':
            self.stopTimer()
            self.rate -= 2
            if self.rate <= 0:
                self.rate = 1
                return
            self.startTimer()
        elif text == 'c':
            self.stopTimer()
            self.points = self.points[-5:]
            self.setPlotData(self.points[-1:], -3)
            # print(self.points[-1])
            self.startTimer()
        # elif text == '?':
        #     print('xrange: ', self.axisX().max() - self.axisX().min())
        #     print('yrange: ', self.axisY().max() - self.axisY().min())
        #     print('size: ', self.size())imer()

        else:
            event.ignore()


if __name__ == '__main__':
    from sys import argv, exit

    try:
        for fig_class in figures:
            if fig_class.__name__ == argv[1]:
                break
        else:
            exit(f'{argv[0]}: {argv[1]}: unknown Figure.\n'
                 f'Please specify one of:\n'
                 f'\t{" ".join(x.__name__ for x in figures)}')
    except IndexError:
        fig_class = figures[-1]  # better a default than a required arg

    if argv[2:]:
        fig = fig_class(*[float(x) for x in argv[2:]])
    else:
        fig = fig_class()  # figure classes should have default geometries

    app = QApplication(argv)

    pointStart = (.3, .3)
    points = array([pointStart])
    angle = angleStart = rand() * 2 * pi
    billiard = GuiBilliard(fig)

    mw = QMainWindow()
    mw.setGeometry(100, 50, 800, 700)
    mw.setWindowTitle('Billiard via PyQtPlot')
    mw.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    billiard.setup(pointStart, angleStart)
    mw.setCentralWidget(billiard.chartView)
    mw.statusBar().showMessage("Press 'q' to quit; Space to run/pause; "
                               "'f' to single-step (when paused); "
                               "'+ / -' to speed up / slow down; "
                               "'c' to clear track")
    mw.show()
    # billiard.startTimer()

    exit(app.exec())
