#! /usr/bin/env python3
'''Demo static Billiard trajectories on various Figures.
'''

from numpy import pi, zeros, cos, sin, empty, concatenate
from matplotlib.pyplot import axis, plot, show, subplots, style
from numpy.random import rand
from figures import *
from billiard import Billiard


def test_figure(figure_class_name, *args):
    for f in figures:
        if f.__name__ == figure_class_name:
            figure_class = f
            break
    else:
        raise ValueError(f'test_figure: {figure_class_name} is wrong')
    fig = figure_class(*args)
    ((xmin, ymin), (xmax, ymax)) = fig.bounds
    P = empty((50000, 2))
    P[:, 0] = xmin - .1 + (xmax - xmin + .2) * rand(50000)
    P[:, 1] = ymin - .1 + (ymax - ymin + .2) * rand(50000)
    inside = zeros(50000, dtype='bool')
    for k in range(50000):
        if fig.is_inside(P[k]):
            inside[k] = True
    P_in = P[inside, :]
    P_out = P[~inside, :]
    axis('equal')
    plot(P_in[:, 0], P_in[:, 1], 'r,')
    plot(P_out[:, 0], P_out[:, 1], 'b,')
    if isinstance(fig.border, list):
        for b in fig.border:
            plot(b[:, 0], b[:, 1], 'k-')
    else:
        b = fig.border
        plot(b[:, 0], b[:, 1], 'k-')
    show()


def demo(fig):
    fig = fig.lower()
    if fig == 'circle':
        fig = Circle(1.)
        point_start = [.5, 0.]
        angle = .3
    elif fig == 'rectangle':
        fig = Rectangle([0, 0], [1, 1])
        point_start = [.96, .1]
        angle = pi / 4 - .1
    elif fig == 'sinai':
        fig = Sinai(2., .15)
        point_start = [.75, 0.]
        angle = .1
    elif fig == 'stadium':
        fig = Stadium(1., 1.)
        point_start = [.5, .5]
        angle = 0.11
    elif fig == 'ring':
        fig = Ring(2., .5, [.4, 0.])
        point_start = [-1., .5]
        angle = 2.75 * pi / 4
    elif fig == 'eight':
        fig = Eight()
        point_start = [-.75, .2]
        angle = .11
    elif fig == 'ellipse':
        fig = Ellipse()
        point_start = fig.foci[0]
        angle = .11
    nsteps = 9999
    billiard = Billiard(fig)
    # points, angle = billiard.run(point_start, angle, nsteps)
    t, a = billiard.run(point_start, angle)
    tracks = [t]
    for _ in range(nsteps - 1):
        t, a = billiard.run(t[-1], a)
        tracks.append(t)
    points = concatenate(tracks)
    angle = a
    f, ax = subplots()
    ax.set_aspect('equal')
    ax.set_frame_on(False)
    ax.axis('off')
    ax.plot(points[:, 0], points[:, 1], 'b:')
    if isinstance(fig.border, list):
        for b in fig.border:
            ax.plot(b[:, 0], b[:, 1], 'k.')
    else:
        b = fig.border
        ax.plot(b[:, 0], b[:, 1], 'k.')
    ax.plot(point_start[0], point_start[1], 'ko')
    ax.arrow(points[-1, 0], points[-1, 1], .1 * cos(angle), .1 * sin(angle),
             head_width=.07)
    ax.set_title('{} Billiard'.format(fig.__class__.__name__))
    show()


if __name__ == '__main__':
    from sys import argv
    style.use('seaborn-notebook')
    if 'demo' in argv[1:]:
        fun = demo
        argv.remove('demo')
    else:
        fun = test_figure
    if len(argv) > 1:
        for name in argv[1:]:
            fun(name)
    else:
        for name in ('circle', 'rectangle', 'sinai', 'stadium', 'ring'):
            fun(name)
