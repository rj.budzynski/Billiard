#! /usr/bin/env python3
'''Animated display of the Billiard via matplotlib.
'''

from figures import figures
from billiard import Billiard
from matplotlib.pyplot import subplots, show
from matplotlib.animation import FuncAnimation
from matplotlib.patches import FancyArrow
import matplotlib.style as style
from numpy import array, sin, cos, concatenate, pi
from numpy.random import rand


def init_anim():
    ax.set_aspect('equal')
    ax.set_frame_on(False)
    ax.axis('off')
    lines = []
    if isinstance(fig.border, list):
        for b in fig.border:
            lines.extend(ax.plot(b[:, 0], b[:, 1], 'k.'))
    else:
        b = fig.border
        lines.extend(ax.plot(b[:, 0], b[:, 1], 'k.'))
    lines.extend(ax.plot(point_start[0], point_start[1], 'ko'))
    points, angle = billiard.run(point_start, angle_start, 1)
    lines.extend(ax.plot(points[:, 0], points[:, 1], 'b:'))
    ax.set_title("{} Billiard : click to start / pause; (q)uit".format(
        fig.__class__.__name__))
    return lines


def init():
    return lines[-1],


def animate(k):
    global points, angle
    line = lines[-1]
    more_points, angle = billiard.run(points[-1], angle, 50)
    points = concatenate((points, more_points))
    line.set_xdata(points[:, 0])
    line.set_ydata(points[:, 1])
    for o in ax.findobj(FancyArrow):
        o.remove()
    arr = ax.arrow(
        points[-1, 0], points[-1, 1], .01 * cos(angle), .01 * sin(angle),
        head_width=.02, head_starts_at_zero=False, color='red')
    if not k:
        anim.event_source.stop()
    return line, arr


def on_click(event):
    global running
    if running:
        anim.event_source.stop()
        running = False
    else:
        anim.event_source.start()
        running = True


def on_q(event):
    if event.key == 'q':
        exit()


if __name__ == '__main__':
    from sys import argv, exit
    try:
        for fig_class in figures:
            if fig_class.__name__ == argv[1]:
                break
        else:
            exit(f'{argv[0]}: {argv[1]}: unknown Figure.\n'
                 f'Please specify one of:\n'
                 f'\t{" ".join(x.__name__ for x in figures)}'
                 )
    except IndexError:
        # exit(f'{argv[0]}: a figure name argument is required.')
        fig_class = figures[-1]  # better a default than a required arg

    fig = fig_class()  # figure classes should have default geometries
    billiard = Billiard(fig)
    point_start = (.3, .3)
    points = array([point_start])
    # angle = angle_start = .19
    angle = angle_start = rand() * 2 * pi

    style.use('seaborn-poster')
    f, ax = subplots()

    running = False
    #f.canvas.set_window_title("Billiard Animation")
    f.canvas.mpl_connect('button_press_event', on_click)
    f.canvas.mpl_connect('key_press_event', on_q)
    lines = init_anim()
    anim = FuncAnimation(f, animate, init_func=init, blit=True, interval=200)
    # blitting cuts the cpu load in this specific animation by about 1/2
    # (even if the load isn't huge anyway)
    # anim.save('billiard.gif', writer='imagemagick')
    show()
