# A simple, yet extensible simulation of planar billiards on various figures

For some background, see  [Wikipedia](https://en.wikipedia.org/wiki/Dynamical_billiards).

Required:

- Python >= 3.6
- NumPy
- For `animate.py`: Matplotlib
- For `pganimate.py`: PyQtGraph, Qt (tested with PyQt5)
- For `qtanimate.py`: PyQtCharts (pip3 install)

Hint: `apt install python3-pyqtgraph`; if your system lacks apt, you probably know how to work around that shortcoming (anaconda or the like).

If you want to see animations, run `animate.py` or `pganimate.py`.
Try `animate.py help` or `pganimate.py` for a list of available figures.

The code is still messy, hopefully will improve.

You are free to do whatever you please with this code. Attribution will be appreciated. Let me know if you find it of any use.

## Notes

- PyQtGraph is a pleasant surprise; it's not too bad to work with, and quite capable and efficient
- QtCharts is a disappointment; though it produces nice looking plots, it's cumbersome to code and the animation turns out to be surprisingly cpu-hungry; some common tasks require jumping through lots of hoops, some I haven't been able to solve (setting aspect ratio?)
- Maplotlib has slow startup, and making it look decent is a chore, but performance is not too bad

----
© 2018 Robert J Budzyński <Robert.Budzynski@fuw.edu.pl>