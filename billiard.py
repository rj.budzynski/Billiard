#! /usr/bin/env python3

from numpy import array, all
from math import cos, sin, hypot  # math functions are faster on scalars
# btw to compute a sin(x) is faster than a cache (dict) lookup

STEP = .003
PRECISION = .0001


def distance(x, y):
    # return sqrt(((x - y) ** 2).sum())
    return hypot(*(x - y))  # silly microoptimization, valid for 2d only


class Billiard(object):
    def __init__(self, figure):
        self.figure = figure

    def locate_boundary(self, low, high, precision=PRECISION):
        is_outside = self.figure.is_outside
        while distance(low, high) > precision:
            x = (high + low) / 2
            if is_outside(x):
                high = x
            else:
                low = x
        return low

    def next_point(self, point, angle, step=STEP):
        if self.figure.is_outside(point):
            raise ValueError(
                'point {} is outside while should be inside'
                .format(point))
        nextp = point + step * array((cos(angle), sin(angle)))
        θ = angle
        if self.figure.is_outside(nextp):
            boundary_point = self.locate_boundary(point, nextp)
            θ = self.figure.reflection_angle(boundary_point, angle)
            nextp = boundary_point + .1 * step * array((cos(θ), sin(θ)))

            # ARRGH, now point can be both outside and on the boundary
            while self.figure.is_outside(nextp):
                nextp = (boundary_point + nextp) / 2
                if all(nextp == boundary_point):
                    raise RuntimeError('in Billiard.next_point() !'
                                       f'point: {nextp}; θ: {θ}')
            # prevents the ball getting stuck?
        return nextp, θ

    def run(self, point_start, angle, nsteps=1, step=STEP):
        nextp, nextangle = self.next_point(
                            point_start, angle, step=step * nsteps)
        points = [point_start, nextp]
        if nextangle != angle:
            d = step * nsteps - distance(point_start, nextp)
            p, nextnextangle = self.next_point(nextp, nextangle, step=d)
            points.append(p)
            angle = nextnextangle
        # print('\n'.join(repr(p) for p in points))
        return array(points), angle


__all__ = ['STEP', 'PRECISION', 'Billiard']
